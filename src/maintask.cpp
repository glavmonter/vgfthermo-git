/*
 * maintask.cpp
 *
 *  Created on: 04 марта 2015 г.
 *      Author: supervisor
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "hardware.h"
#include "lmp90100.h"
#include "tlsf.h"
#include "iadc.h"
#include "common.h"
#include "eeprom.h"
#include "diag/Trace.h"
#include "maintask.h"
#include "cantask.h"
#include "pidng.h"

static portTASK_FUNCTION_PROTO(vMainTask, pvParameters);

extern SemaphoreHandle_t xCanMutex;

void InitSystem() {
	xTaskCreate(vMainTask, "Main", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY, NULL);
}

static portTASK_FUNCTION(vMainTask, pvParameters) {
(void) pvParameters;

	//EEPROMInit();
	//RTCInit();

uint8_t DeviceId = ENCGetID();

	portENTER_CRITICAL();
		xCanMutex = xSemaphoreCreateMutex();
		xSemaphoreGive(xCanMutex);

		CANTask *canTask = new CANTask("CAN", configTASK_CAN_PRIORITY, configMINIMAL_STACK_SIZE*4);
		assert_param(canTask);
		CANLink(canTask);

		PIDInit();

		ADCsSetupSPI();

		LMP90100 *lmpADC_0_3 = new LMP90100("ADC1", configTASK_ADC_PRIORITY,
								configMINIMAL_STACK_SIZE*4, ADC1_SPI);
		assert_param(lmpADC_0_3);
		LMPLink(LMPIndex::LMPIndex_0_3, lmpADC_0_3);

		LMP90100 *lmpADC_4_7 = new LMP90100("ADC2", configTASK_ADC_PRIORITY,
								configMINIMAL_STACK_SIZE*4, ADC2_SPI);
		assert_param(lmpADC_4_7);
		LMPLink(LMPIndex::LMPIndex_4_7, lmpADC_4_7);

		PidNg *pid = NULL;
		/* Для младшего АЦП, зоны 0 - 3 */
		for (uint8_t z = 0; z < 4; z++) {
			pid = new PidNg(z, DeviceId, configTASK_PID_PRIORITY, configMINIMAL_STACK_SIZE*4);
			assert_param(pid);

			canTask->m_lPids[z] = pid;
			lmpADC_0_3->m_lPids[z] = pid;
		}

		/* Для старшего АЦП, зоны 4 - 7 */
		for (uint8_t z = 4; z < 8; z++) {
			pid = new PidNg(z, DeviceId, configTASK_PID_PRIORITY, configMINIMAL_STACK_SIZE*4);
			assert_param(pid);

			canTask->m_lPids[z] = pid;
			lmpADC_4_7->m_lPids[z - 4] = pid;
		}

		canTask->m_lADCs[0] = lmpADC_0_3;
		canTask->m_lADCs[1] = lmpADC_4_7;

		canTask->m_lPids[0]->CCR = (uint32_t *) &TIM8->CCR3;
		canTask->m_lPids[1]->CCR = (uint32_t *) &TIM8->CCR4;
		canTask->m_lPids[2]->CCR = (uint32_t *) &TIM8->CCR1;
		canTask->m_lPids[3]->CCR = (uint32_t *) &TIM8->CCR2;
		canTask->m_lPids[4]->CCR = (uint32_t *) &TIM1->CCR3;
		canTask->m_lPids[5]->CCR = (uint32_t *) &TIM1->CCR4;
		canTask->m_lPids[6]->CCR = (uint32_t *) &TIM1->CCR1;
		canTask->m_lPids[7]->CCR = (uint32_t *) &TIM1->CCR2;
	portEXIT_CRITICAL();

	for (;;) {
		vTaskDelay(portMAX_DELAY);
	}
}
