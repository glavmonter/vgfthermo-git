/*
 * pidng.cpp
 *
 *  Created on: 04 марта 2015 г.
 *      Author: supervisor
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <timers.h>
#include <arm_math.h>
#include <core_cm4.h>
#include <diag/Trace.h>
#include "hardware.h"
#include "pb.h"
#include "pb_encode.h"
#include "canmsg.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "VGFThermo.pb.h"
#include "Thermocouple.h"
#include "iadc.h"
#include "pidng.h"
#include "SEGGER_RTT.h"


/**
 * @function PIDInit
 * @brief Запуск таймеров
 * @param None
 * @retval None
 */
void PIDInit() {
GPIO_InitTypeDef GPIO_InitStructure;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 | RCC_APB2Periph_TIM8, ENABLE);

	GPIO_InitStructure.GPIO_Pin = TIM1_CH0_PIN | TIM1_CH1_PIN | TIM1_CH2_PIN | TIM1_CH3_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(TIM1_CH0_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = TIM8_CH0_PIN | TIM8_CH1_PIN | TIM8_CH2_PIN | TIM8_CH3_PIN;
	GPIO_Init(TIM8_CH0_GPIO_PORT, &GPIO_InitStructure);

	/* TIM1 AF settings */
	GPIO_PinAFConfig(TIM1_CH0_GPIO_PORT, TIM1_CH0_SOURCE, TIM1_CH0_AF);
	GPIO_PinAFConfig(TIM1_CH1_GPIO_PORT, TIM1_CH1_SOURCE, TIM1_CH1_AF);
	GPIO_PinAFConfig(TIM1_CH2_GPIO_PORT, TIM1_CH2_SOURCE, TIM1_CH2_AF);
	GPIO_PinAFConfig(TIM1_CH3_GPIO_PORT, TIM1_CH3_SOURCE, TIM1_CH3_AF);

	/* TIM8 AF settings */
	GPIO_PinAFConfig(TIM8_CH0_GPIO_PORT, TIM8_CH0_SOURCE, TIM8_CH0_AF);
	GPIO_PinAFConfig(TIM8_CH1_GPIO_PORT, TIM8_CH1_SOURCE, TIM8_CH1_AF);
	GPIO_PinAFConfig(TIM8_CH2_GPIO_PORT, TIM8_CH2_SOURCE, TIM8_CH2_AF);
	GPIO_PinAFConfig(TIM8_CH3_GPIO_PORT, TIM8_CH3_SOURCE, TIM8_CH3_AF);

	/* 915 Hz */
	TIM_TimeBaseStructure.TIM_Prescaler = 2;	// 180 MHz/3 = 60 MHz
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF;  // ARR = 65535, 915 Hz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;	// TODO Test center aligned PWM
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);

	/* TIM1 Channel1 duty cycle = (TIM1_CCR1/ TIM1_ARR)* 100 = 50% */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1Init(TIM8, &TIM_OCInitStructure);

	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC2Init(TIM1, &TIM_OCInitStructure);
	TIM_OC2Init(TIM8, &TIM_OCInitStructure);

	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC3Init(TIM1, &TIM_OCInitStructure);
	TIM_OC3Init(TIM8, &TIM_OCInitStructure);

	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);
	TIM_OC4Init(TIM8, &TIM_OCInitStructure);

	TIM_Cmd(TIM1, ENABLE);
	TIM_Cmd(TIM8, ENABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_CtrlPWMOutputs(TIM8, ENABLE);
}

PidNg::PidNg(uint8_t ZoneNumber, uint8_t DeviceID, unsigned portBASE_TYPE priority, uint16_t stackDepth) {
char buf[configMAX_TASK_NAME_LEN];
	m_iZoneNumber = ZoneNumber;
	m_iRealZoneNumber = ZoneNumber + (DeviceID - 1)*8;
	this->DeviceId = DeviceID;

	snprintf(buf, configMAX_TASK_NAME_LEN, "PID_%d", ZoneNumber);
	m_xQueueADCIn = xQueueCreate(2, sizeof(PidNg::ADCIn));
	assert_param(m_xQueueADCIn);

	m_xQueueSettingIn = xQueueCreate(1, sizeof(HostToDevice));
	assert_param(m_xQueueSettingIn);

	m_xQueueSet = xQueueCreateSet(2+1);
	assert_param(m_xQueueSet);
	xQueueAddToSet(m_xQueueADCIn, m_xQueueSet);
	xQueueAddToSet(m_xQueueSettingIn, m_xQueueSet);

	m_xQueueCurrentState = xQueueCreate(1, sizeof(DeviceToHost));
	assert_param(m_xQueueCurrentState);

	m_xSemaphoreSync = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreSync);
	xSemaphoreGive(m_xSemaphoreSync);


	UpdateDtH();

	m_xPid.Kp = 0.0f;
	m_xPid.Ki = 0.0f;
	m_xPid.Kd = 0.0f;
	pid_init(&m_xPid, true);

	xTaskCreate(task_pid, buf, stackDepth, this, priority, &handle);
	assert_param(handle);
}

void PidNg::UpdateDtH() {
DeviceToHost dth;
	dth.ZoneNumber = m_iZoneNumber;
	dth.CurrentT = m_fCurrentT;
	dth.PresetT = m_fPresetT;
	dth.PwmOut = m_iPwmOut;
	dth.PidError = m_fPidError;
	dth.Sendiag = m_iSendiag;
	dth.Mode = m_xRegulatorMode;
	xQueueOverwrite(m_xQueueCurrentState, &dth);
}

void PidNg::pid_init(PidNg::pid_instance *inst, bool reset) {
	/* Derived coefficient A0 */
	inst->A0 = inst->Kp + inst->Ki + inst->Kd;

	/* Derived coefficient A1 */
	inst->A1 = (-inst->Kp) - ((float32_t) 2.0 * inst->Kd);

	/* Derived coefficient A2 */
	inst->A2 = inst->Kd;


	/* Check whether state needs reset or not */
	if (reset) {
		inst->state[0] = 0.0f;
		inst->state[1] = 0.0f;
		inst->state[2] = 0.0f;
	}
}

float PidNg::pid_calc(PidNg::pid_instance *inst, float datain) {
	float out;

		/* y[n] = y[n-1] + A0 * x[n] + A1 * x[n-1] + A2 * x[n-2]  */
		out = 	(inst->A0 * datain) +
				(inst->A1 * inst->state[0]) +
				(inst->A2 * inst->state[1]) +
				(inst->state[2]);

	    /* Update state */
		inst->state[1] = inst->state[0];
		inst->state[0] = datain;
		inst->state[2] = out;

	    /* return to application */
		return out;
}


void PidNg::ProcessPid(PidNg::ADCIn *data) {
(void) data;
}


void PidNg::ProcessSettings(HostToDevice *settings) {
	m_fPresetT = settings->has_PresetT ? settings->PresetT : m_fPresetT;
	m_iPwmMaximum = settings->has_PwmMaximum ? settings->PwmMaximum : m_iPwmMaximum;
	m_iPwmOut = settings->has_PwmOut ? settings->PwmOut : m_iPwmOut;

	m_xPid.Kp = settings->has_Kp ? settings->Kp : m_xPid.Kp;
	m_xPid.Ki = settings->has_Ki ? settings->Ki : m_xPid.Ki;
	m_xPid.Kd = settings->has_Kd ? settings->Kd : m_xPid.Kd;

	if (settings->has_Kp || settings->has_Ki || settings->has_Kd) {
		pid_init(&m_xPid);
	}

	if (settings->ResetPid) {
		pid_init(&m_xPid, true);
	}

	m_xRegulatorMode = settings->has_Mode ? settings->Mode : m_xRegulatorMode;
}



void PidNg::ProcessPid(float T) {
	if (m_iSendiag == DeviceToHost_SendiagFlags_SENDIAG_NORMAL) {
		m_fCurrentT = m_xFilter.Process(T);
		float deltaT = m_fPresetT - m_fCurrentT;

		int32_t pwm;

		switch (m_xRegulatorMode) {
		case RegulatorMode_MODE_STOPPED:
			*CCR = 0;
			break;

		case RegulatorMode_MODE_MANUAL:
			*CCR = m_iPwmOut;
			break;

		case RegulatorMode_MODE_RELAY:
			if (deltaT > 0.0001f) {
				*CCR = m_iPwmMaximum;
				m_iPwmOut = m_iPwmMaximum;
			} else {
				*CCR = 0;
				m_iPwmOut = 0;
			}
			break;

		case RegulatorMode_MODE_PID:
			m_fPidError = pid_calc(&m_xPid, deltaT);
			pwm = roundf(m_fPidError);

			if (pwm < 0)
				m_iPwmOut = 0;
			else if ((uint32_t)pwm > m_iPwmMaximum)
				m_iPwmOut = m_iPwmMaximum;
			else
				m_iPwmOut = pwm;
			*CCR = m_iPwmOut;
			break;

		case RegulatorMode_MODE_ACCIDENT:
			*CCR = 0;
			break;
		}
	} else {
	}

	UpdateDtH();
}


void PidNg::task() {
CanTxMsg TxMessageEmc;
	TxMessageEmc.StdId = GENID(DeviceId, CANMSG_EMERGENCY);
	TxMessageEmc.ExtId = 0;
	TxMessageEmc.IDE = CAN_Id_Standard;
	TxMessageEmc.RTR = CAN_RTR_Data;
	TxMessageEmc.DLC = 2;
	TxMessageEmc.Data[0] = m_iZoneNumber;

	for (;;) {
		QueueSetHandle_t activateMember = xQueueSelectFromSet(m_xQueueSet, 5000);

		if ((activateMember != NULL) && (xSemaphoreTake(m_xSemaphoreSync, 0) == pdTRUE)) {
			m_iCurrentTime++;
		}

		if (activateMember == m_xQueueADCIn) {
			PidNg::ADCIn ddin;
			if (xQueueReceive(m_xQueueADCIn, &ddin, 0) == pdFALSE) {
				TxMessageEmc.DLC = 2;
				TxMessageEmc.Data[0] = m_iZoneNumber;
				TxMessageEmc.Data[1] = 1;
				CanSendSem(&TxMessageEmc);
			}

			if (m_iZoneNumber < 4)
				LED1_PORT->ODR ^= LED1_PIN;
			else
				LED2_PORT->ODR ^= LED2_PIN;

			m_iSendiag = ddin.sendiag;
			if (ddin.adc_data_uv > 52410.0) {
				ProcessPid(1300.0f);
				*CCR = 0;
			} else {
				ProcessPid(GetTemperature(ddin.adc_data_uv));
			}

		} else if (activateMember == m_xQueueSettingIn) {
			HostToDevice htd;
			if (xQueueReceive(m_xQueueSettingIn, &htd, 0) == pdFALSE) {
				TxMessageEmc.DLC = 2;
				TxMessageEmc.Data[0] = m_iZoneNumber;
				TxMessageEmc.Data[1] = 2;
				CanSendSem(&TxMessageEmc);
			}
			ProcessSettings(&htd);

		} else {
			m_iTimeoutError++;
			TxMessageEmc.DLC = 4;
			TxMessageEmc.Data[0] = m_iZoneNumber;
			TxMessageEmc.Data[1] = 3;
			TxMessageEmc.Data[2] = (m_iTimeoutError & 0xFF00) >> 8;
			TxMessageEmc.Data[3] = m_iTimeoutError & 0x00FF;
			CanSendSem(&TxMessageEmc);
		}
	}
}



FilterKalmanNg::FilterKalmanNg() {
}

float FilterKalmanNg::Process(float data_in) {
float x_temp_est;
float P_temp;
float K;

	x_temp_est = x_est_last;
	P_temp = P_last + Q;
	K = P_temp*(1.0f/(P_temp + R));
	x_est = x_temp_est + K*(data_in - x_temp_est);

	P_last = (1.0f - K)*P_temp;
	x_est_last = x_est;

	return x_est;
}
