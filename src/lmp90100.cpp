/*
 *  @file VGFThermo/lmp90100.c
 *  @date 24.12.2012
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief LMP90100 AFE routings.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <stm32f4xx.h>
#include "hardware.h"
#include "config.h"
#include "canmsg.h"
#include "diag/Trace.h"
#include "lmp90100.h"


static LMP90100 *lmpADC_0_3 = NULL;
static LMP90100 *lmpADC_4_7 = NULL;


#define __SPI_GET_FLAG(__HANDLE__, __FLAG__) ((((__HANDLE__)->SR) & (__FLAG__)) == (__FLAG__))

/** @brief  Clear the SPI OVR pending flag.
  * @param  __HANDLE__: specifies the SPI handle.
  *         This parameter can be SPI where x: 1, 2, or 3 to select the SPI peripheral.
  * @retval None
  */
#define __SPI_CLEAR_OVRFLAG(__HANDLE__) do{(__HANDLE__)->DR;\
                                               (__HANDLE__)->SR;}while(0)

static void SPI_Transmit(SPI_TypeDef *SPIx, uint8_t *pData, uint16_t Size, uint32_t Timeout) {
uint8_t *pTxBuffPtr = pData;
uint16_t TxXferSize = Size;

TickType_t nextTick;

	SPIx->DR = (*pTxBuffPtr++);
	TxXferSize--;
	while (TxXferSize > 0) {
		nextTick = xTaskGetTickCount() + Timeout;
		while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_TXE) == RESET) && (xTaskGetTickCount() < nextTick)) {}
		SPIx->DR = (*pTxBuffPtr++);
		TxXferSize--;
	}

	nextTick = xTaskGetTickCount() + Timeout;
	while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_TXE) == RESET) && (xTaskGetTickCount() < nextTick)) {}

	nextTick = xTaskGetTickCount() + Timeout;
	while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_BSY) == SET) && (xTaskGetTickCount() < nextTick)) {}

	__SPI_CLEAR_OVRFLAG(SPIx);
}

static void SPI_TransmitReceive(SPI_TypeDef *SPIx, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout) {
uint8_t *pTxBuffPtr = pTxData;
uint8_t *pRxBuffPtr = pRxData;
uint16_t TxXferCount = Size;
uint16_t RxXferCount = Size;
TickType_t nextTick;

	SPIx->DR = (*pTxBuffPtr++);
	TxXferCount--;

	if(TxXferCount == 0) {

		/* Wait until RXNE flag is set */
		nextTick = xTaskGetTickCount() + Timeout;
		while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_RXNE) == RESET) && (xTaskGetTickCount() < nextTick)) {}

		(*pRxBuffPtr) = SPIx->DR;
		RxXferCount--;

	} else {

		while (TxXferCount > 0) {
			/* Wait until TXE flag is set to send data */
			nextTick = xTaskGetTickCount() + Timeout;
			while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_TXE) == RESET) && (xTaskGetTickCount() < nextTick)) {}

			SPIx->DR = (*pTxBuffPtr++);
			TxXferCount--;


			/* Wait until RXNE flag is set */
			nextTick = xTaskGetTickCount() + Timeout;
			while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_RXNE) == RESET) && (xTaskGetTickCount() < nextTick)) {}

			(*pRxBuffPtr++) = SPIx->DR;
			RxXferCount--;
		}

		/* Wait until RXNE flag is set */
		nextTick = xTaskGetTickCount() + Timeout;
		while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_RXNE) == RESET) && (xTaskGetTickCount() < nextTick)) {}
		(*pRxBuffPtr++) = SPIx->DR;
		RxXferCount--;
	}

	nextTick = xTaskGetTickCount() + Timeout;
	while ((__SPI_GET_FLAG(SPIx, SPI_FLAG_BSY) == SET) && (xTaskGetTickCount() < nextTick)) {}

	__SPI_CLEAR_OVRFLAG(SPIx);
}


/**
 * @function ADCsSetupSPI
 * Инициализация SPI2 и SPI3
 */
void ADCsSetupSPI() {
GPIO_InitTypeDef 	GPIO_InitStructure;
SPI_InitTypeDef		SPI_InitStructure;
EXTI_InitTypeDef	EXTI_InitStructure;
NVIC_InitTypeDef	NVIC_InitStructure;

	/* Enable SPI Clock */
	ADC1_SPI_CLK_INIT(ADC1_SPI_CLK, ENABLE);
	ADC2_SPI_CLK_INIT(ADC2_SPI_CLK, ENABLE);

	/* Connect SPI pins to AF5 (SPI2) */
	GPIO_PinAFConfig(ADC1_SPI_SCK_GPIO_PORT, ADC1_SPI_SCK_SOURCE, ADC1_SPI_SCK_AF);
	GPIO_PinAFConfig(ADC1_SPI_MISO_GPIO_PORT, ADC1_SPI_MISO_SOURCE, ADC1_SPI_MISO_AF);
	GPIO_PinAFConfig(ADC1_SPI_MOSI_GPIO_PORT, ADC1_SPI_MOSI_SOURCE, ADC1_SPI_MOSI_AF);

	/* Connect SPI pins to AF6 (SPI3) */
	GPIO_PinAFConfig(ADC2_SPI_SCK_GPIO_PORT, ADC2_SPI_SCK_SOURCE, ADC2_SPI_SCK_AF);
	GPIO_PinAFConfig(ADC2_SPI_MISO_GPIO_PORT, ADC2_SPI_MISO_SOURCE, ADC2_SPI_MISO_AF);
	GPIO_PinAFConfig(ADC2_SPI_MOSI_GPIO_PORT, ADC2_SPI_MOSI_SOURCE, ADC2_SPI_MOSI_AF);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	/* SPI2 SCK */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_SCK_PIN;
	GPIO_Init(ADC1_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	/* SPI2 MISO */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_MISO_PIN;
	GPIO_Init(ADC1_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	/* SPI2 MOSI */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_MOSI_PIN;
	GPIO_Init(ADC1_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 SCK */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_SCK_PIN;
	GPIO_Init(ADC2_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 MISO */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_MISO_PIN;
	GPIO_Init(ADC2_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 MOSI */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_MOSI_PIN;
	GPIO_Init(ADC2_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);


	/* SPI2 NSS */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_NSS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(ADC1_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_NSS_PIN;
	GPIO_Init(ADC2_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);

	/* DRDY pins */
	GPIO_InitStructure.GPIO_Pin = ADC1_DRDY_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(ADC1_DRDY_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ADC2_DRDY_PIN;
	GPIO_Init(ADC2_DRDY_GPIO_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Connect EXTI Line1 to PD1 */
	SYSCFG_EXTILineConfig(ADC1_DRDY_EXTI_SOURCE, ADC1_DRDY_EXTI_PIN);
	EXTI_InitStructure.EXTI_Line = EXTI_Line1;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI Line1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x05;  // Высший приоритет
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	/* Connect EXTI Line 0 to PD0 */
	SYSCFG_EXTILineConfig(ADC2_DRDY_EXTI_SOURCE, ADC2_DRDY_EXTI_PIN);
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI Line0 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x05;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	GPIOB->BSRRL = GPIO_Pin_12;
	GPIOA->BSRRL = GPIO_Pin_15;


	SPI_I2S_DeInit(ADC1_SPI);
	SPI_I2S_DeInit(ADC2_SPI);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	SPI_Init(ADC1_SPI, &SPI_InitStructure);
	SPI_Cmd(ADC1_SPI, ENABLE);

	SPI_Init(ADC2_SPI, &SPI_InitStructure);
	SPI_Cmd(ADC2_SPI, ENABLE);
}


void LMPLink(LMPIndex index, LMP90100 *l) {
	switch (index) {

	case LMPIndex::LMPIndex_0_3:
		lmpADC_0_3 = l;
		break;

	case LMPIndex::LMPIndex_4_7:
		lmpADC_4_7 = l;
		break;

	default:
		break;
	}
}

/**
  * @brief  This function handles External line 1 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI1_IRQHandler(void) {
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(EXTI->PR & EXTI_Line1) {
		/* Clear the EXTI line 1 pending bit */
		EXTI->PR = EXTI_Line1;

		xSemaphoreGiveFromISR(lmpADC_0_3->xSemaphoreISR, &xHigherPriorityTaskWoken);
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/**
  * @brief  This function handles External line 0 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI0_IRQHandler(void) {
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(EXTI->PR & EXTI_Line0) {
		/* Clear the EXTI line 0 pending bit */
		EXTI->PR = EXTI_Line0;

		xSemaphoreGiveFromISR(lmpADC_4_7->xSemaphoreISR, &xHigherPriorityTaskWoken);
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}



LMP90100::LMP90100(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth, SPI_TypeDef *spi):
		xSemaphoreISR(NULL), m_uURA(LMP90100_URA_END), adc_timeout_error(0), adc_crc_error(0) {

	m_pSPI = spi;

	xSemaphoreISR = xSemaphoreCreateBinary();
	assert_param(xSemaphoreISR);
	xSemaphoreTake(xSemaphoreISR, 0);
	xQueueTCold = xQueueCreate(1, sizeof(m_fTCold));

	xTaskCreate(task_adc, name, stackDepth, this, priority, &handle);
	assert_param(handle);
}

LMP90100::~LMP90100() {

}

#define CH_DATA_SIZE 	5	// 5 bytes: Status, ADC_DOUT2, ADC_DOUT1, ADC_DOUT0, CRC

void LMP90100::task() {
	WriteReg(0x00, 0xC3);
	WriteRegSettings();

uint8_t addr;
	addr = LMP90100_SENDIAG_FLAGS_REG;
	EnableDataFirstMode(addr, CH_DATA_SIZE);

CanTxMsg TxMessageEmc;
	TxMessageEmc.StdId = GENID(ENCGetID(), CANMSG_EMERGENCY);
	TxMessageEmc.ExtId = 0;
	TxMessageEmc.IDE = CAN_Id_Standard;
	TxMessageEmc.RTR = CAN_RTR_Data;
	TxMessageEmc.DLC = 1;

uint8_t id = (m_pSPI == ADC1_SPI) ? 10 : 11;
uint32_t to = 0;

	for (;;) {
		if (xSemaphoreTake(xSemaphoreISR, 5000) == pdTRUE) {

			uint8_t data[CH_DATA_SIZE];

			NSS_LOW();
				SPI_TransmitReceive(m_pSPI, data, data, CH_DATA_SIZE, 10);
			NSS_HIGH();

			if (CRCCheck(data, CH_DATA_SIZE - 1) == CRC_PASS) {
				int32_t adc_data = ((uint32_t)data[1] << 16) | ((uint32_t)data[2] << 8) | (uint32_t)data[3];
				uint8_t ch_num = data[0] & LMP90100_CH_NUM_MASK;
				uint8_t sendiag = data[0] & 0xF8;

				if (adc_data & 0x800000)
					adc_data = -(((~adc_data) & 0xFFFFFF) + 1);

				double adc_data_uV = ((double)adc_data * 2500000.0)/(8388608.0*32.0);

				PidNg::ADCIn msg;
				msg.adc_data_uv = adc_data_uV + m_fTCold;
				msg.sendiag = sendiag;
				if (xQueueSend(m_lPids[ch_num]->m_xQueueADCIn, &msg, 5) != pdTRUE) {
					TxMessageEmc.DLC = 3;
					TxMessageEmc.Data[0] = id;
					TxMessageEmc.Data[1] = 2;
					TxMessageEmc.Data[2] = ch_num;
					CanSendSem(&TxMessageEmc);
				}

				/* Получаем данные о холодном конце */
				if (uxQueueMessagesWaiting(xQueueTCold) > 0)
					xQueueReceive(xQueueTCold, &m_fTCold, 1);

				/* Просто для проверки работы */
				if (to == 750) { // 750 = 60 sec
					to = 0;
					TxMessageEmc.DLC = 2;
					TxMessageEmc.Data[0] = id;
					TxMessageEmc.Data[1] = 255;
					CanSendSem(&TxMessageEmc);
				} else {
					to += 1;
				}

			} else {
				/* CRC Error */
				adc_crc_error++;
				TxMessageEmc.DLC = 6;
				TxMessageEmc.Data[0] = id;
				TxMessageEmc.Data[1] = 3;
				TxMessageEmc.Data[2] = m_iCrcCalc;
				TxMessageEmc.Data[3] = m_iCrcRcv;
				TxMessageEmc.Data[4] = (adc_crc_error & 0xFF00) >> 8;
				TxMessageEmc.Data[5] = adc_crc_error & 0x00FF;
				CanSendSem(&TxMessageEmc);
			}

		} else { // xSemaphoreTake(xSemaphoreISR, 5000) == pdFALSE
			/* Timeout waiting semaphore */
			adc_timeout_error++;

			TxMessageEmc.DLC = 4;
			TxMessageEmc.Data[0] = id;
			TxMessageEmc.Data[1] = 5;
			TxMessageEmc.Data[2] = (adc_timeout_error & 0xFF00) >> 8;
			TxMessageEmc.Data[3] = adc_timeout_error & 0x00FF;
			CanSendSem(&TxMessageEmc);
		}
	}
}


/*
 * Запись значения во внутренний регистр АЦП
 */
void LMP90100::WriteReg(uint8_t addr, uint8_t value) {
uint8_t newURA, inst;
	newURA = (addr & LMP90100_URA_MASK) >> 4;

	NSS_LOW();

	if (m_uURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;
		SPI_Transmit(m_pSPI, &inst, 1, SPI_TIMEOUT);
		SPI_Transmit(m_pSPI, &newURA, 1, SPI_TIMEOUT);
		m_uURA = newURA;
	}

	inst = LMP90100_WRITE_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	SPI_Transmit(m_pSPI, &inst, 1, SPI_TIMEOUT);
	SPI_Transmit(m_pSPI, &value, 1, SPI_TIMEOUT);

	NSS_HIGH();
}


/*
 * Чтение значения из внутреннего регистра АЦП
 */
uint8_t LMP90100::ReadReg(uint8_t addr) {
uint8_t newURA, inst, rxdata;
	newURA = (addr & LMP90100_URA_MASK) >> 4;

	NSS_LOW();
	if (m_uURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;
		SPI_Transmit(m_pSPI, &inst, 1, SPI_TIMEOUT);
		SPI_Transmit(m_pSPI, &newURA, 1, SPI_TIMEOUT);
		m_uURA = newURA;
	}

	inst = LMP90100_READ_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	SPI_Transmit(m_pSPI, &inst, 1, SPI_TIMEOUT);
	SPI_TransmitReceive(m_pSPI, &rxdata, &rxdata, 1, SPI_TIMEOUT);

	NSS_HIGH();
	return rxdata;
}

inline void LMP90100::NSS_LOW() {
	if (m_pSPI == SPI2)
		GPIOB->BSRRH = GPIO_Pin_12;
	else if (m_pSPI == SPI3)
		GPIOA->BSRRH = GPIO_Pin_15;
}

inline void LMP90100::NSS_HIGH() {
	if (m_pSPI == SPI2)
		GPIOB->BSRRL = GPIO_Pin_12;
	else if (m_pSPI == SPI3)
		GPIOA->BSRRL = GPIO_Pin_15;
}

/*
 * Все, что нужно для вычисления CRC
 */
uint8_t LMP90100::crc8MakeBitwise2(uint8_t crc, uint8_t poly, uint8_t *pmsg, uint8_t msg_size) {
uint8_t msg;
	for (int i = 0 ; i < msg_size ; i++) {
		msg = (*pmsg++ << 0);
		for (int j = 0 ; j < 8 ; j++) {
			if((msg ^ crc) >> 7)
				crc = (crc << 1) ^ poly;
			else
				crc <<= 1;
			msg <<= 1;
		}
	}
	return(crc ^ CRC8_FINAL_XOR);
}

uint8_t LMP90100::CRCCheck (uint8_t *buffer, uint8_t count) {

	m_iCrcRcv = buffer[count];                                                    // extract CRC data
	m_iCrcCalc = crc8MakeBitwise2(CRC8_INIT_REM, CRC8_POLY,
                                buffer, count);                     // calculate CRC for the adc output (size 3 bytes)
	if (m_iCrcCalc == m_iCrcRcv)
		return CRC_PASS;
	else
		return CRC_FAIL;
}

void LMP90100::WriteRegSettings() {

	WriteReg(	LMP90100_RESETCN_REG,
					LMP90100_RESETCN_REG_VALUE);
	WriteReg(	LMP90100_SPI_HANDSHAKECN_REG,
					LMP90100_SPI_HANDSHAKECN_REG_VALUE);
	WriteReg(	LMP90100_SPI_STREAMCN_REG,
					LMP90100_SPI_STREAMCN_REG_VALUE);
	WriteReg(	LMP90100_PWRCN_REG,
					LMP90100_PWRCN_REG_VALUE);
	WriteReg(	LMP90100_ADC_RESTART_REG,
					LMP90100_ADC_RESTART_REG_VALUE);
	WriteReg( 	LMP90100_GPIO_DIRCN_REG,
					LMP90100_GPIO_DIRCN_REG_VALUE);
	WriteReg( 	LMP90100_GPIO_DAT_REG,
					LMP90100_GPIO_DAT_REG_VALUE);
	WriteReg( 	LMP90100_BGCALCN_REG,
					LMP90100_BGCALCN_REG_VALUE);
	WriteReg( 	LMP90100_SPI_DRDYBCN_REG,
					LMP90100_SPI_DRDYBCN_REG_VALUE);
	WriteReg( 	LMP90100_ADC_AUXCN_REG,
					LMP90100_ADC_AUXCN_REG_VALUE);
	WriteReg( 	LMP90100_SPI_CRC_CN_REG,
					LMP90100_SPI_CRC_CN_REG_VALUE);
	WriteReg( 	LMP90100_SENDIAG_THLDH_REG,
					LMP90100_SENDIAG_THLDH_REG_VALUE);
	WriteReg( 	LMP90100_SENDIAG_THLDL_REG,
					LMP90100_SENDIAG_THLDL_REG_VALUE);
	WriteReg(	LMP90100_SCALCN_REG,
					LMP90100_SCALCN_REG_VALUE);
	WriteReg(	LMP90100_ADC_DONE_REG,
					LMP90100_ADC_DONE_REG_VALUE);

TickType_t nextTick;
	nextTick = xTaskGetTickCount() + 1000; // Не более 1000 тиков читаем статус DAC
	while ((ReadReg(LMP90100_CH_STS_REG) & LMP90100_CH_SCAN_NRDY) && (xTaskGetTickCount() < nextTick)) {}

	WriteReg( 	LMP90100_CH_SCAN_REG,
					LMP90100_CH_SCAN_REG_VALUE);

	WriteReg(	LMP90100_CH0_INPUTCN_REG,
					LMP90100_CH0_INPUTCN_REG_VALUE);
	WriteReg(	LMP90100_CH0_CONFIG_REG,
					LMP90100_CH0_CONFIG_REG_VALUE);

	WriteReg(	LMP90100_CH1_INPUTCN_REG,
					LMP90100_CH1_INPUTCN_REG_VALUE);
	WriteReg(	LMP90100_CH1_CONFIG_REG,
					LMP90100_CH1_CONFIG_REG_VALUE);

	WriteReg(	LMP90100_CH2_INPUTCN_REG,
					LMP90100_CH2_INPUTCN_REG_VALUE);
	WriteReg(	LMP90100_CH2_CONFIG_REG,
					LMP90100_CH2_CONFIG_REG_VALUE);

	WriteReg(	LMP90100_CH3_INPUTCN_REG,
					LMP90100_CH3_INPUTCN_REG_VALUE);
	WriteReg(	LMP90100_CH3_CONFIG_REG,
					LMP90100_CH3_CONFIG_REG_VALUE);
}


/**
 * @function LMP90100_SPIEnableDataFirstMode
 * Enables the data first mode of LMP90100.
 */
void LMP90100::EnableDataFirstMode(uint8_t addr, uint8_t count) {
	WriteReg(LMP90100_DATA_ONLY_1_REG, addr);
	WriteReg(LMP90100_DATA_ONLY_2_REG, count - 1);


uint8_t txdata;
uint8_t rxdata;

	NSS_LOW();
	txdata = LMP90100_DATA_FIRST_MODE_INSTRUCTION_ENABLE;
	SPI_Transmit(m_pSPI, &txdata, 1, SPI_TIMEOUT);

	txdata = LMP90100_DATA_FIRST_MODE_INSTRUCTION_READ_MODE_STATUS;
	SPI_TransmitReceive(m_pSPI, &txdata, &rxdata, 1, SPI_TIMEOUT);

TickType_t nextTick = xTaskGetTickCount() + 1000;
	while ((rxdata & LMP90100_DATA_FIRST_MODE_STATUS_FLAG) && (xTaskGetTickCount() < nextTick)) {
		SPI_TransmitReceive(m_pSPI, &txdata, &rxdata, 1, SPI_TIMEOUT);
	}

	NSS_HIGH();
}
