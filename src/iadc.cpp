/*
 * iadc.cpp
 *
 *  Created on: 15 oct 2014.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <new>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "diag/Trace.h"
#include "tlsf.h"
#include "common.h"
#include "stm32f4xx_conf.h"
#include "iadc.h"

static IADC *iadc = NULL;

void IADCLink(IADC *ll) {

	iadc = ll;
}

void IADCInit() {
	iadc = new IADC("IADC", tskIDLE_PRIORITY + 2, configMINIMAL_STACK_SIZE * 2);
}


/*
 * ADC1_IN16 - Температура кристалла
 */

IADC::IADC(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth): cnt(0) {

	/* ADC1 только измерение температуры кристалла */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);


	TIM_DeInit(TIM3);
	TIM_TimeBaseInitStructure.TIM_Period = 399;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 2625-1;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);

	TIM_SelectOutputTrigger(TIM3, TIM_TRGOSource_Update);
	TIM_SelectMasterSlaveMode(TIM3, TIM_MasterSlaveMode_Disable);

	datas_temp = (uint16_t *)pvPortMalloc(sizeof(uint16_t) * NUM_TEMPERATURES);
	memset(datas_temp, 0, sizeof(uint16_t)*NUM_TEMPERATURES);

	DMA_DeInit(DMA2_Stream0);
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)datas_temp;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = NUM_TEMPERATURES;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0, &DMA_InitStructure);
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);


NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F; // Самый низший приоритет
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T3_TRGO;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;

	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_15Cycles);
	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);

	ADC_DMACmd(ADC1, ENABLE);

	xMutexTemperature = xSemaphoreCreateMutex();

	xDMASemph = xSemaphoreCreateBinary();

	xTaskCreate(task_iadc, name, stackDepth, this, priority, &handle);
}


IADC::~IADC() {
#if INCLUDE_vTaskDelete
	vTaskDelete(this->handle);
#endif
}


bool IADC::GetCPUTemperature(float *T) {
	if (cnt == 9) {
		/* Выдаем температуру проца каждые 10 синков */
		cnt = 0;

		if (T == NULL)
			return false;

		if (xSemaphoreTake(xMutexTemperature, 10) != pdTRUE)
			return false;

		*T = TemperatureCPU;
		xSemaphoreGive(xMutexTemperature);
		return true;
	}

	cnt++;
	return false;
}


void IADC::task() {
	xSemaphoreTake(xDMASemph, 1);

	ADC_Cmd(ADC1, ENABLE);
	DMA_Cmd(DMA2_Stream0, ENABLE);
	TIM_Cmd(TIM3, ENABLE);


	for (;;) {
		if (xSemaphoreTake(xDMASemph, portMAX_DELAY) == pdTRUE) {
			uint32_t averageT = 0;
			for (uint32_t i = 0; i < NUM_TEMPERATURES; i++) {
				averageT += datas_temp[i];
				datas_temp[i] = 0;
			}


			/* Очищаем флаги Overrange и еще какой-то */
			ADC1->SR = ~(uint32_t)(ADC_FLAG_OVR | ADC_FLAG_STRT);
			DMA2_Stream0->NDTR = (uint32_t)NUM_TEMPERATURES;
			DMA2_Stream0->M0AR = (uint32_t)datas_temp;
			DMA2_Stream0->CR |= DMA_SxCR_EN;

			TIM3->CR1 |= TIM_CR1_CEN;

			float T = (float)averageT/NUM_TEMPERATURES;
			T = ((T*3300/0xfff/1000.0f)-0.760f)/0.0025f+25.0f;
			if (xSemaphoreTake(xMutexTemperature, 10) == pdTRUE) {
				TemperatureCPU = T;
				xSemaphoreGive(xMutexTemperature);
			}
		}
	}
}

void DMA2_Stream0_IRQHandler() {
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (DMA2->LISR & DMA_IT_TCIF0) {
		DMA2->LIFCR = DMA_IT_TCIF0;
		TIM3->CR1 &= (uint16_t)~TIM_CR1_CEN;

		xSemaphoreGiveFromISR(iadc->xDMASemph, &xHigherPriorityTaskWoken);
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
