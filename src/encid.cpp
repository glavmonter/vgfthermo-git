/*
 * encid.c
 *
 *  Created on: 18.02.2013
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */

#include <stdint.h>
#include <stm32f4xx.h>
#include "hardware.h"
#include "encid.h"


void ENCInit() {
GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = ENC_S1_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(ENC_S1_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ENC_S2_PIN;
	GPIO_Init(ENC_S2_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ENC_S4_PIN;
	GPIO_Init(ENC_S4_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ENC_S8_PIN;
	GPIO_Init(ENC_S8_GPIO_PORT, &GPIO_InitStructure);
}

uint8_t ENCGetID() {
uint8_t ret;
	ret  = (GPIO_ReadInputDataBit(ENC_S1_GPIO_PORT, ENC_S1_PIN) == Bit_SET) ? (1 << 0) : 0;
	ret |= (GPIO_ReadInputDataBit(ENC_S2_GPIO_PORT, ENC_S2_PIN) == Bit_SET) ? (1 << 1) : 0;
	ret |= (GPIO_ReadInputDataBit(ENC_S4_GPIO_PORT, ENC_S4_PIN) == Bit_SET) ? (1 << 2) : 0;
	ret |= (GPIO_ReadInputDataBit(ENC_S8_GPIO_PORT, ENC_S8_PIN) == Bit_SET) ? (1 << 3) : 0;
	return ret;
}

