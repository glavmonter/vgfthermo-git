/*
 * cantask.cpp
 *
 *  Created on: 27 апр. 2015 г.
 *      Author: user5
 */

#include "SEGGER_RTT.h"
#include "cantask.h"

static CANTask *canTask = NULL;


void CANLink(CANTask *task) {
	assert_param(task);
	canTask = task;
}



CANTask::CANTask(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth): CanBase() {
	xTaskCreate(task_can, name, stackDepth, this, priority, &handle);
	assert_param(handle);
}

CANError CANTask::ProcessPkt() {
CanTxMsg TxMessageEmc;

	TxMessageEmc.StdId = GENID(DeviceID, CANMSG_EMERGENCY);
	TxMessageEmc.ExtId = 0;
	TxMessageEmc.IDE = CAN_Id_Standard;
	TxMessageEmc.RTR = CAN_RTR_Data;
	TxMessageEmc.DLC = 1;

	for (uint8_t z = 0; z < m_xPayload.HtD_count; z++) {
		if (xQueueSend(m_lPids[m_xPayload.HtD[z].ZoneNumber]->m_xQueueSettingIn,
				&m_xPayload.HtD[z], 10) == pdFALSE) {

			TxMessageEmc.DLC = 3;
			TxMessageEmc.Data[0] = 9;
			TxMessageEmc.Data[1] = 2;
			TxMessageEmc.Data[2] = z;
			CanSendSem(&TxMessageEmc);
		}
	}

	if (m_xPayload.has_TCold) {
		xQueueOverwrite(m_lADCs[0]->xQueueTCold, &m_xPayload.TCold);
		xQueueOverwrite(m_lADCs[1]->xQueueTCold, &m_xPayload.TCold);
	}
	return CANError::CAN_OK;
}


CANError CANTask::EncodeProto(pb_ostream_t *stream) {
CanTxMsg TxMessageEmc;

	TxMessageEmc.StdId = GENID(DeviceID, CANMSG_EMERGENCY);
	TxMessageEmc.ExtId = 0;
	TxMessageEmc.IDE = CAN_Id_Standard;
	TxMessageEmc.RTR = CAN_RTR_Data;
	TxMessageEmc.DLC = 1;

	m_xPayload = Payload_init_default;
	for (uint32_t i = 0; i < ZONES_COUNT; i++) {
		if (xQueuePeek(m_lPids[i]->m_xQueueCurrentState, &m_xPayload.DtH[i], 10) == pdFALSE) {
			TxMessageEmc.DLC = 3;
			TxMessageEmc.Data[0] = 9;
			TxMessageEmc.Data[1] = 1;
			TxMessageEmc.Data[2] = i;
			CanSendSem(&TxMessageEmc);
		}
	}
	m_xPayload.DtH_count = ZONES_COUNT;
	m_xPayload.DeviceId = DeviceID;

	if (!pb_encode(stream, Payload_fields, &m_xPayload)) {
		return CANError::CAN_ERROR_PB;
	}
	return CANError::CAN_OK;
}

CANError CANTask::DecodeProto(pb_istream_t *stream) {
	if (!pb_decode(stream, Payload_fields, &m_xPayload))
		return CANError::CAN_ERROR_PB;
	return CANError::CAN_OK;

}

void vTimerCallback(TimerHandle_t pxTimer) {
(void) pxTimer;
	NVIC_SystemReset();
}

void CANTask::task() {
CanRxMsg RxMsg;

TimerHandle_t xTimer = xTimerCreate("Reset", 5000, pdFALSE, (void *)0, vTimerCallback);

	for (;;) {
		if (xQueueReceive(xCanQueueRx, &RxMsg, 1000) == pdTRUE) {
			switch (GETMSG(RxMsg.StdId)) {
			case CANMSG_SYNC:
				/* Передаем сигнал синхронизации в Регуляторы */
				for (int i = 0; i < ZONES_COUNT; i++) {
					xSemaphoreGive(m_lPids[i]->m_xSemaphoreSync);
				}

				if (RxMsg.DLC == 0) {
					SendToHost();
				}
				else if (RxMsg.Data[0] == 0xFA) {
					/* Тут делаем сброс на загрузчик */
					RTC_WriteBackupRegister(RTC_BKP_DR0, 0xAABBCCDD);
					xTimerStart(xTimer, 0);
				}
				break;

			case CANMSG_HTD:
			case CANMSG_DATA:
				if (ReceiveFromHost(&RxMsg) == CANError::CAN_PKT_READY)
					ProcessPkt();
				break;

			default:
				break;
			}
		} else {
//			VirtualSendToHost();
		}
	}
}



void CAN1_RX0_IRQHandler(void) {
CanRxMsg RxMessage;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	CAN_Receive(CANLIB_CAN, CAN_FIFO0, &RxMessage);
	xQueueSendFromISR(canTask->xCanQueueRx, &RxMessage, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
