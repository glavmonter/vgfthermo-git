/**
  ******************************************************************************
  * @file    VGFThermo/main.c
  * @author  Vladimir Meshkov <glavmonter@gmail.com>
  * @version V1.0.0
  * @date    4-December-2012
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "hardware.h"
#include "canmsg.h"
#include "encid.h"
#include "tlsf.h"
#include "iadc.h"
#include "common.h"
#include "eeprom.h"
#include "diag/Trace.h"
#include "maintask.h"
#include "SEGGER_RTT.h"

/* Private typedef -----------------------------------------------------------*/
SemaphoreHandle_t xMutexSegger;

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile unsigned long ulRunTimeStatsClock;

/* Private function prototypes -----------------------------------------------*/
static portTASK_FUNCTION_PROTO(vLEDBlink, pvParameters);
static portTASK_FUNCTION_PROTO(vIWDGTask, pvParameters);

static void prvSetupHardware();
static void prvStartIwdt();

#if configGENERATE_RUN_TIME_STATS == 1
static void setupRunTimeStats();
#endif

/* Private functions ---------------------------------------------------------*/
static char pool_ram[configTOTAL_HEAP_SIZE] __attribute__((aligned(8)));

__attribute__ ((section(".flash_data_sect")))
__IO const uint32_t flash_var = 0xCC1122EE;

__attribute__ ((section(".flash_data_sect")))
__IO const uint32_t flash_var_2 = 0x11223344;

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void) {
	SystemCoreClockUpdate();
	init_memory_pool(configTOTAL_HEAP_SIZE, pool_ram);

	prvSetupHardware();
#if configGENERATE_RUN_TIME_STATS == 1
	setupRunTimeStats();
#endif
	xMutexSegger = xSemaphoreCreateMutex();
	SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
	SEGGER_RTT_printf(0, "Hello\n");

	prvStartIwdt();
	InitSystem();

TaskHandle_t handle = NULL;


//	xTaskCreate(vLEDBlink, "Stat", configMINIMAL_STACK_SIZE*3, NULL, tskIDLE_PRIORITY, &handle);
//	assert_param(handle);

	xTaskCreate(vIWDGTask, "IWDG", configMINIMAL_STACK_SIZE*1, NULL, tskIDLE_PRIORITY, &handle);
	assert_param(handle);
	vTaskStartScheduler();

	/* Infinite loop */
	while (1)
	{}
	return 0;
}

static void prvStartIwdt() {
	/* Enable the LSI oscillator ************************************************/
	RCC_LSICmd(ENABLE);
	/* Wait till LSI is ready */
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET){}

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_32); /* min - 1 ms, max - 4096 ms */
	IWDG_SetReload(0xFFF); // max
	IWDG_ReloadCounter();
	IWDG_Enable();
}

/*
 *
 */
#if configGENERATE_RUN_TIME_STATS == 1

static void setupRunTimeStats() {
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = (uint16_t)(SystemCoreClock/20000) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM7, ENABLE);
}

void TIM7_IRQHandler() {
	if (TIM_GetITStatus(TIM7, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
		ulRunTimeStatsClock++;
	}
}
#endif



static void prvSetupHardware() {

	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
							RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
							RCC_AHB1Periph_GPIOE, ENABLE);

GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = LED1_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(LED1_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED2_PIN;
	GPIO_Init(LED2_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_Init(LED3_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED4_PIN;
	GPIO_Init(LED4_PORT, &GPIO_InitStructure);

	LED_GREEN(LED1);
	LED_GREEN(LED2);
	LED_GREEN(LED3);
	LED_GREEN(LED4);
	ENCInit();

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	/* PA9 - USART1_Tx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
}


static portTASK_FUNCTION(vIWDGTask, pvParameters) {
(void)pvParameters;
	for (;;) {
		IWDG_ReloadCounter();
		vTaskDelay(2000);
	}
	vTaskDelete(NULL);
}

#if configGENERATE_RUN_TIME_STATS == 1
char buffer[48*20];
portTASK_FUNCTION(vLEDBlink, pvParameters) {
(void)pvParameters;
	for (;;) {
		vTaskDelay(5000);
		LED3_PORT->ODR ^= LED3_PIN;

		vTaskGetRunTimeStats(buffer);
		SEGGER_RTT_printf(0, "\n===========================================\n");
		SEGGER_RTT_printf(0, buffer);
		SEGGER_RTT_printf(0, "===========================================\n\n");
	}
}
#else
static portTASK_FUNCTION(vLEDBlink, pvParameters) {
(void)pvParameters;

	for (;;) {
		vTaskDelay(100);
		LED3_PORT->ODR ^= LED3_PIN;
	}
}
#endif

void vApplicationStackOverflowHook(TaskHandle_t handle, char *name) {
(void)handle;

CanTxMsg TxMessage;
	TxMessage.StdId = GENID(ENCGetID(), CANMSG_EMERGENCY);
	TxMessage.ExtId = 0;
	TxMessage.IDE = CAN_Id_Standard;
	TxMessage.RTR = CAN_RTR_Data;
	TxMessage.DLC = 8;
	TxMessage.Data[0] = 8;
	TxMessage.Data[1] = name[0];
	TxMessage.Data[2] = name[1];
	TxMessage.Data[3] = name[2];
	TxMessage.Data[4] = name[3];
	TxMessage.Data[5] = name[4];
	TxMessage.Data[6] = name[5];
	TxMessage.Data[7] = name[6];

	CanSendSem(&TxMessage);
	for (;;){}
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
