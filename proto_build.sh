#!/bin/bash

protoc -I. -I /usr/include/ -I ../nanopb/generator/proto/ -I ../protos/ -o VGFThermo.pb ../protos/VGFThermo_nano.proto
python ../nanopb/generator/nanopb_generator.py VGFThermo.pb
mv *.pb.h include
mv *.pb.c src

rm *.pb
