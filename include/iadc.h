#ifndef IADC_H_
#define IADC_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include "common.h"


class IADC;
void IADCInit();
void IADCLink(IADC *ll);


#define NUM_TEMPERATURES	80

class IADC : public TaskBase {
public:
	IADC(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);
	~IADC();

	void task();
	static void task_iadc(void *param) {
		static_cast<IADC *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

public:
	bool GetCPUTemperature(float *T);
	SemaphoreHandle_t xDMASemph;

private:
	uint16_t *datas_temp;

	DMA_InitTypeDef 		DMA_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	ADC_InitTypeDef 		ADC_InitStructure;
	ADC_CommonInitTypeDef 	ADC_CommonInitStructure;


	/* Результирующие значения температуры */
	float TemperatureCPU;
	SemaphoreHandle_t xMutexTemperature;

	uint32_t cnt;
};

#ifdef __cplusplus
extern "C" {
#endif

void DMA2_Stream0_IRQHandler();

#ifdef __cplusplus
}
#endif

#endif /* PID_H_ */
