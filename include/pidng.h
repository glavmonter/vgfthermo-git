/*
* pidng.h
 *
 *  Created on: 04 марта 2015 г.
 *      Author: supervisor
 */

#ifndef PIDNG_H_
#define PIDNG_H_

#include <arm_math.h>
#include <stm32f4xx.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <semphr.h>

#include "common.h"
#include "VGFThermo.pb.h"
#include "encid.h"

void PIDInit();

class FilterKalmanNg {
public:
	FilterKalmanNg();
//	FilterKalman(float q, float r, float xEstLast, float PLast, float xEst);

	float Q = 0.001f;
	float R = 0.01f;
	float Process(float data_in);

private:
	float x_est_last = 0.0f;
	float P_last = 24.0f;
	float x_est = 0.0f;
};


class PidNg : public TaskBase {
public:
	typedef struct {
		double adc_data_uv;
		uint8_t sendiag;
	} ADCIn;

public:
	PidNg(uint8_t ZoneNumber, uint8_t DeviceID, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_pid(void *param) {
		static_cast<PidNg *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t m_xQueueADCIn;
	QueueHandle_t m_xQueueSettingIn;
	QueueHandle_t m_xQueueCurrentState;
	SemaphoreHandle_t m_xSemaphoreSync;

	uint32_t *CCR;

private:
	QueueSetHandle_t m_xQueueSet;
	uint8_t m_iZoneNumber;
	uint8_t m_iRealZoneNumber;

	uint32_t m_iTimeoutError = 0;

	void ProcessPid(PidNg::ADCIn *data);
	void ProcessPid(float T);
	void ProcessSettings(HostToDevice *settings);
	void UpdateDtH();

private:
	uint8_t DeviceId;

	FilterKalmanNg m_xFilter;

	float m_fCurrentT = 20.0f;
	float m_fPresetT = 20.0f;
	uint32_t m_iPwmMaximum = 0;
	uint32_t m_iPwmOut = 0;

	float m_fPidError = 0.0f;
	uint32_t m_iSendiag = DeviceToHost_SendiagFlags_SENDIAG_NORMAL;
	RegulatorMode m_xRegulatorMode = RegulatorMode_MODE_STOPPED;

	/*************************************************************************/
	uint32_t m_iCurrentTime = 0;
	/*************************************************************************/

	typedef struct {
		float A0;          /**< The derived gain, A0 = Kp + Ki + Kd . */
		float A1;          /**< The derived gain, A1 = -Kp - 2Kd. */
		float A2;          /**< The derived gain, A2 = Kd . */
		float state[3];    /**< The state array of length 3. */
		float Kp;          /**< The proportional gain. */
		float Ki;          /**< The integral gain. */
		float Kd;          /**< The derivative gain. */
	} pid_instance;

	PidNg::pid_instance m_xPid;
	void pid_init(PidNg::pid_instance *inst, bool reset = false);
	float pid_calc(PidNg::pid_instance *i, float d);
};



#endif /* PIDNG_H_ */
