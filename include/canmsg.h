/*
 * canmsg.h
 *  Список всех возможных сообщений CAN
 *  Created on: 04.12.2012
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */

#ifndef CANMSG_H_
#define CANMSG_H_

#include <stdint.h>

/*
 * Структура Arbitration field (11 bit):
 * 7 верхних бит - тип сообщения, нижние 4 - ID получателя
 */

/* Верхние 7 бит */
#define CANMSG_EMERGENCY	0x07	// нижние 4 бит - 0, максимальный приоритет, 1 DLC
#define CANMSG_SYNC			0x08	// нижние 4 бит - 0, приоритет пониже emcy, 0 DLC
#define CANMSG_ACK			0x09	// нижние 4 бит - id куда, 0 DLC
#define CANMSG_NACK			0x0A	// нижние 4 бит - id куда, 0 DLC
#define CANMSG_DTH			0x0B	// нижние 4 бит - id куда, 1-4 DLC
#define CANMSG_HTD			0x0C	// нижние 4 бит - id откуда, 1-4 DLC
#define CANMSG_HEARTBEAT	0x0D	// нижние 4 бит - id откуда, 0 DLC
#define CANMSG_DATA			0x0E	// нижние 4 бит - id куда, ? DLC


/* Нижние 4 бита, id куда-откуда */
#define CANID_BROADCAST		0x0		// 0b0000
#define CANID_MASTER		0xF		// 0b1111

#define GENID(ID, MSG) 	((MSG << 4) | ID)
#define GETMSG(cobid) 	((cobid >> 4) & 0x0F)
#define GETID(cobid)	(cobid & 0x0F)

#endif /* CANMSG_H_ */
