/*
 * cantask.h
 *
 *  Created on: 27 апр. 2015 г.
 *      Author: user5
 */

#ifndef CANTASK_H_
#define CANTASK_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <timers.h>
#include <stm32f4xx.h>

#include "common.h"
#include "canmsg.h"
#include "hardware.h"
#include "encid.h"
#include "pb.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "pidng.h"
#include "lmp90100.h"
#include "VGFThermo.pb.h"
#include "canbase.hpp"

#if (Payload_size > 7*256)
#error "Payload too large"
#endif

#define ZONES_COUNT		8



class CANTask : public TaskBase, public CanBase<Payload_size, Payload> {
public:
	CANTask(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_can(void *param) {
		static_cast<CANTask *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	PidNg *m_lPids[ZONES_COUNT];
	LMP90100 *m_lADCs[2];

	CANError EncodeProto(pb_ostream_t *stream);
	CANError DecodeProto(pb_istream_t *stream);

private:
	CANError ProcessPkt();
};


void CANLink(CANTask *task);


#ifdef __cplusplus
extern "C" {
#endif

void CAN1_RX0_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* CANTASK_H_ */
